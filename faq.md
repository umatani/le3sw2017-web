# FAQ (よくある質問とその回答)

## 1. OCaml環境構築編

* Q1-1. 実験ホームページからリンクの張られている「OCaml環境設定情報（講義「プログラミング言語」ホームページの一番下）」の手順に従って`opam switch 4.03.0`を実行しようとするとエラーになります．
    - A1-1. シェルの alias 設定が有効になっていない可能性があります．OSにログインしなおすなどしてみてください．
* Q1-2. `opam`の設定が有効になりません．
    - A1-2. 演習室のUbuntu等で`bash`シェルを使っている場合，シェルの環境設定ファイルは`opam init`がデフォルトで想定している`~/.bash_profile`ではなく`~/.profile`です．たとえば：
    ```bash
    $ opam init
    ...(中略)...
    Do you want OPAM to modify ~/.bash_profile and ~/.ocamlinit?
    (default is 'no', use 'f' to name a file other than ~/.bash_profile)
        [N/y/f] f
      Enter the name of the file to update: ~/.profile
    ```
    のように`opam init`コマンドを実行している途中で入力し，`opam init`が`~/.profile`に設定を加えるように指定してください．
* Q1-3. Emacsの中で`run-ocaml`が使えない．
    - A1-3. Emacsの設定ファイル中に：
    ```Lisp
    (with-eval-after-load
        (setq tuareg-interactive-program "~/.opam/opam config exec -- ocaml"))
    ```
    と書いてください．Emacsの設定ファイルがどこにあるかは人によって異なりますが，`~/.emacs`，`~/.emacs.d/init.el`などが一般的です．

* Q1-4. opamの各種コマンンドを実行するとスタックオーバーフローで落ちる．
    - A1-4. opamサービス側の問題のようです．とりあえず，自分のシェルの設定ファイル(`~/.bash_profile`，`~/.profile`等)の末尾に：
    ```
    CAML_LD_LIBRARY_PATH="/export/home/lab4/k.yasugi/.opam/4.03.0/lib/stublibs"
    export CAML_LD_LIBRARY_PATH
    PERL5LIB="/export/home/lab4/k.yasugi/.opam/4.03.0/lib/perl5"
    export PERL5LIB
    OCAML_TOPLEVEL_PATH="/export/home/lab4/k.yasugi/.opam/4.03.0/lib/toplevel"
    export OCAML_TOPLEVEL_PATH
    PATH="/export/home/lab4/k.yasugi/.opam/4.03.0/bin:$PATH"
    export PATH
    ```
    を追加し，ログインしなおすと課題2や課題3の`make`は出来るようになるはずです．
    （ただし，`opam install`，`opam update`，`opam upgrade`などのopam環境を更新するよう
    なコマンドは一切使えません．定期的に上の設定をコメントアウトして，元の（自分の）`opam`コマンド
    がまた使えるようになってないか確認し，使えるようになってたら，ここで説明した回避策の使用は控えてください．

## 2. OCaml入門編

* Q2-1. 『Objective Caml入門 』Exercise 6.9中の「学籍番号」とは何ケタですか？
    - A2-1. 下4ケタとします．(全桁で求めた強者が過去にいたという情報あり)

## 3. インタプリタ・型推論(共通事項)編

* Q3-1. `parser.mly` (あるいは`lexer.mll`)を正しく直したのに，課題提出システムの自動テストを通りません．
    - A3-1. 提出システムにアップロードする際，zip ファイルを生成する前に必ず`make clean`を実行してください．そうしないと古い`parser.ml`や`lexer.ml`が悪影響を及ぼす可能性があります．

## 4. インタプリタ編

* Q4-1. Exercise 3.2.3の論理演算子（`&&`，`||`）を正しく実装したはずなのに，自動テストを通りません．
    - A4-1. `&&`，`||`の正確な定義を誤解している可能性があります．『Objective Caml入門 』2.2.6節の説明をもう一度よく読みなおしてみてください．

## 5. 型推論編

* Q5-1. 自分の手元では型推論器が型を表示しているのに，自動テストが1つも通りません．
    - A-5.1 OCaml準拠な型の表現でなければテストを通過しないようになっています．たとえば：
    ```
    # fun x -> fun y -> x;;
    − : 'a -> 'b -> 'a = <fun>
    # fun f -> fun x -> f x;;
    − : ('a -> 'b) -> 'a -> 'b = <fun>
    ```
    と表示されることを確認してください．
## 6. その他編

* Q6-1. 課題提出システムで意味不明なエラーが出てしまいます．ブラウザはSafariです．
    - A6-1. Safariでは課題提出システムが正常に動作しないという症状を，こちらでもいくつか確認してはいるのですが，根本的解決には至っていません．すみませんが，上手くいかない場合には，最もトラブルの少ない(ように見える)Chromeをインストールし，それを使うようにしてください．
